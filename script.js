"use strict";

let tabActive = document.querySelectorAll(".tabs-title");
let tabContent = document.querySelectorAll(".tabs-content li");

tabActive.forEach((item) => {
  item.addEventListener('click', () => {

    tabActive.forEach((navLink) => {
      navLink.classList.remove('active');
    });
    item.classList.add('active');

    const tabName = item.getAttribute('data-tab');
    tabContent.forEach((content) => {
      content.style.display = 'none';
    });

    const tabContentActive = document.querySelector(`[data-tab-content="${tabName}"]`);
    tabContentActive.style.cssText = `
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    `
  });
});

